#include <iostream>

void math(int n,int type)
{
    for (int i = type; i < n; i += 2) //if type is 1(odd) it's safe to start printing from 1 since 0 is even number
    {
        std::cout << i << "\n";
    }
}

int main()
{
    const int n = 10;
    int type; // 0 - for even, 1 - for odd

    std::cout << "Input 0 for even numbers or 1 for odd numbers\n";
    std::cin >> type;

    math(n,type);
}
